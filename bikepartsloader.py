# coding=utf-8
from __future__ import absolute_import, unicode_literals
import sys
import os
import time
import urlparse
import urllib
import logging

import requests
from requests_toolbelt.sessions import BaseUrlSession
from requests.exceptions import ConnectionError, RequestException
from bs4 import BeautifulSoup

from bike_parts_loader.celeryapp import app
from bike_parts_loader.db.db_manager import DbManager
from bike_parts_loader.time_logger import TimeLogger


log = logging.getLogger('bike_parts_loader.celerytasks')
log.addHandler(logging.NullHandler())
session = BaseUrlSession('https://www.bike-components.de')
session.proxies = {
    'all': 'http://82.196.12.188:12000'
}
dbm = DbManager()

BASE_URL = 'https://www.bike-components.de'
FILE_PATH = 'dumps'

def debug_dump(soup):
    filename = '{}.html'.format(time.time())
    full_file_path = os.path.abspath(os.path.join(FILE_PATH, 'soup_dump', filename))
    s = soup.prettify()
    with open(full_file_path, 'w') as f:
        f.write(s.encode('utf-8'))


def get_response(url):
    '''Returns HTTP response object from url.'''
    max_retries = 5
    retry_counter = 0
    while True:
        try:
            url = urlparse.urljoin(BASE_URL, url)
            response = requests.get(url, proxies={'all': 'http://localhost:10250'})
            response.raise_for_status()
        except RequestException as e:
            log.exception('request error')
            if retry_counter >= max_retries:
                raise
            retry_counter += 1
        else:
            return response


def get_soup(response):
    '''Converts HTTP response to BeautifulSoup object'''
    return BeautifulSoup(response.text, 'lxml')


def incr_query_param(url, query_param):
    '''

    :param url: URL to modify
    :param query_param: Querydict parameter to increment
    :return: Modified URL string

    Increments chosen query paramenter by 1 and returns new URL
    '''
    parse = urlparse.urlsplit(url)
    qs = urlparse.parse_qs(parse.query)
    try:
        current_value = int(qs[query_param].pop())
        qs[query_param] = int(current_value+1)
    except KeyError:
        qs[query_param] = 2
    qs_string = urllib.urlencode(qs)
    parse_list = list(parse)
    parse_list[3] = qs_string
    return urlparse.urlunsplit(tuple(parse_list))


@app.task(bind=True, max_retries=3, priority=10)
def extract_item_data(self, item_url):
    """
    Gets item from url and if html markup is correct writes it to database.
    """
    tl = TimeLogger('extract_item_data')
    tl.start_timing('http')
    try:
        response = get_response(item_url)
        # response = session.get(item_url)
    except ConnectionError as e:
        self.retry(exc=e)
    tl.stop_timing('http')
    tl.start_timing('soup')
    soup = get_soup(response)
    tl.stop_timing('soup')
    tl.start_timing('extr')
    try:
        main = soup.find('div', id='grid-main')
        breadcrumbs_div = main.find('div', class_='module-breadcrumbs')
        breadcrumbs = [a['href'] for a in breadcrumbs_div('a')][1:]
        item_number = int(main.find('span', itemprop='sku').string.strip())
        name_h1 = main.find('h1')
        name = name_h1.find('span', itemprop='name').string.strip()
        description_html = main.\
            find('div', class_='description').\
            find('div', class_='site-text').\
            prettify()
        images_div = main.find('div', class_='images')
        images = [img['srcset'].split(' ')[0] for img in images_div.find_all('img', class_='site-image')]
        price = float(main.find('meta', itemprop='price')['content'])

        availability = main.find('div', id='module-product-detail-stock').span.string.strip()
    except Exception:
        log.warning('Broken item html', exc_info=True)
        tl.stop_timing('extr')
    else:
        tl.stop_timing('extr')
        item_info = {
            'breadcrumbs': breadcrumbs,
            'item_number': item_number,
            'name': name,
            'description_html': description_html,
            'images': images,
            'price': price,
            'availability': availability
        }
        save_item_to_db.delay(item_info)
    finally:
        tl.write()


@app.task(priority=10, max_retries=1)
def save_item_to_db(self, item_info):
    '''Celery task wrapper for saving item to db'''
    try:
        dbm.save_item_to_db(item_info)
    except Exception as e:
        log.exception('Can\'t save to db')
        self.retry(exc=e)

@app.task(priority=5)
def extract_items_from_page(page_url):
    '''Extracts all individual items from catalog page and starts tasks for item extraction'''
    tl = TimeLogger('extract_items_from_page')
    tl.start_timing('http')
    response = get_response(page_url)
    tl.stop_timing('http')
    if response.status_code == 200:
        tl.start_timing('soup')
        soup = get_soup(response)
        tl.stop_timing('soup')
        tl.start_timing('extr')
        item_divs = soup.find('main').find_all('div', class_='site-product-item-standard')
        item_links = [div.a['href'] for div in item_divs]
        tl.stop_timing('extr')
        tl.start_timing('task_put')
        for item_url in item_links:
            extract_item_data.delay(item_url)

        if not response.history:
            extract_items_from_page.delay(incr_query_param(page_url, 'page'))
        tl.stop_timing('task_put')
        tl.write()


@app.task(priority=1)
def finalise_load():
    '''Waits for extraction to finish and refreshes materialized view.'''
    DbManager.refresh_mv()
    log.info('FINISH: goods_collection')


def extract_links_from_main():
    '''Extracts all category liks from main page and starts each page data extraction'''
    response = get_response('https://www.bike-components.de/en/')
    soup = get_soup(response)
    nav = soup.find('nav', class_='module-navigation-main')
    links = [a['href'] for a in nav('a')]
    map(extract_items_from_page.delay, links)


def run():
    '''start all items collection tasks'''
    log.info('START: goods_collection')
    extract_links_from_main()
    finalise_load.delay()

if __name__ == '__main__':
    run()