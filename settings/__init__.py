# coding=utf-8
import os

class NoStageVariableSet(Exception):
    pass

stage_variable = 'APP_STAGE'

if stage_variable not in os.environ:
    raise NoStageVariableSet('There is no env variable \'{}\''.format(stage_variable))
else:
    if os.environ[stage_variable] == 'dev':
        from .dev import *
    elif os.environ[stage_variable] == 'prod':
        from .prod import *

from .common import *


