# coding=utf-8
import os
import logging.config

FILE_PATH = os.path.abspath(os.path.join(__file__, os.pardir))
MODULE_PATH = os.path.abspath(os.path.join(FILE_PATH, os.pardir))

LOGS_PATH = os.path.join(MODULE_PATH, 'logs')
print 'loading logging conf'
#dev loggers all in one file
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': "%(name)-10s %(asctime)10s %(levelname)-8s: %(message)-s",
            # 'datefmt' : "%d.%b.%Y %H:%M:%S.%f"
        },
        'threading': {
            'format': "%(name)-10s %(asctime)10s %(levelname)-8s %(thread)6s: %(message)-s",
            # 'datefmt' : "%d.%b.%Y %H:%M:%S.%f"
        },
    },
    'handlers': {
        'null': {
            'level': 'DEBUG',
            'class': 'logging.NullHandler',
        },
        'logfile': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.abspath(os.path.join(LOGS_PATH, 'logfile')),
            'maxBytes': 50000000,
            'backupCount': 5,
            'formatter': 'threading',
            'mode': 'a'
        },
        'error_logfile': {
            'level': 'ERROR',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': os.path.abspath(os.path.join(LOGS_PATH, 'error.log')),
            'maxBytes': 50000000,
            'backupCount': 5,
            'formatter': 'threading',
            'mode': 'a'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {
        # '': {
        #     'handlers': ['logfile'],
        #     'level': 'DEBUG',
        # },
        'sqlalchemy': {
            'handlers': ['logfile', 'console'],
            'level': 'INFO'
        },
        'requests': {
            'handlers': ['logfile'],
            'level': 'DEBUG'
        },
        'celery': {
            'handlers': ['logfile'],
            'level': 'INFO'
        },
        'bike_parts_loader': {
            'handlers': ['logfile', 'console'],
            'level': 'DEBUG'
        },
        'psycopg2': {
            'handlers': ['logfile', 'console'],
            'level': 'DEBUG'
        },
    }
}


logging.config.dictConfig(LOGGING)