# coding=utf-8
from kombu import Queue, Exchange

worker_hijack_root_logger = False
worker_prefetch_multiplier = 100  # 100
worker_concurrency = 15  # 15

run_in_thread = False

if run_in_thread:
    task_always_eager = True
    task_eager_propagates = True



task_queues = [
    Queue(name='celery', queue_arguments={'x-max-priority': 20}),
]

# task_routes = {
#     'bike_parts_loader.bikepartsloader.extract_item_data': 'items',
#     'bike_parts_loader.bikepartsloader.extract_items_from_page': 'pages'
# }