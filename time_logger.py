# coding=utf-8
import time
from collections import defaultdict, OrderedDict
import logging
from .settings import dev

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


class TimeLogger(object):
    def __init__(self, task_name):
        self.task_name = task_name
        self.timings = OrderedDict()
        self._timing_data = defaultdict(dict)

    def start_timing(self, timing_name):
        self._timing_data[timing_name]['start_time'] = time.time()

    def stop_timing(self, timing_name):
        try:
            self.timings[timing_name] = (time.time() -
                                         self._timing_data[timing_name]['start_time'])\
                                        * 1000
        except KeyError as e:
            self._timing_data[timing_name]['error'] = 'stop_timing exc: {}'.format(str(e.args))

    def write(self):
        line = 'task: {s.task_name},'.format(s=self)
        timings_str = ', '.join(['{}: {}ms'.format(timing_name, timing_value)
                                 for timing_name, timing_value in self.timings.iteritems()])

        errors = []
        for item in self._timing_data:
            if 'error' in self._timing_data[item]:
                errors.append('{} error: {}'.format(item, self._timing_data[item]['error']))
        error_str = ', '.join(errors)
        log.debug('timings: {} >> {}, {}'.format(line, timings_str, error_str))


