# coding=utf-8

from sqlalchemy import event
from sqlalchemy.ext import compiler
from sqlalchemy.schema import DDLElement, MetaData, Table, DDL, Column


class CreateMaterializedView(DDLElement):
    def __init__(self, name, selectable):
        self.name = name
        self.selectable = selectable


@compiler.compiles(CreateMaterializedView)
def compile(element, compiler, **kw):
    # Could use "CREATE OR REPLACE MATERIALIZED VIEW..."
    # but I'd rather have noisy errors
    return 'CREATE MATERIALIZED VIEW %s AS %s' % (
        element.name,
        compiler.sql_compiler.process(element.selectable, literal_binds=True))


def create_mat_view(name, selectable, metadata):
    _mt = MetaData()  # temp
    t = Table(name, _mt)
    for c in selectable.c:
        t.append_column(Column(c.name, c.type, primary_key=c.primary_key))

    event.listen(
        metadata, 'after_create',
        CreateMaterializedView(name, selectable)
    )

    @event.listens_for(metadata, "after_create")
    def create_indexes(target, connection, **kw):
        for idx in t.indexes:
            idx.create(connection)

    event.listen(
        metadata, 'before_drop',
        DDL('DROP MATERIALIZED VIEW IF EXISTS ' + name)
    )
    return t


def refresh_mat_view(name, concurrently, session):
    session.flush()
    _con = 'CONCURRENTLY ' if concurrently else ''
    session.execute('REFRESH MATERIALIZED VIEW ' + _con + name)
