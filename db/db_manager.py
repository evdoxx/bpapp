# coding=utf-8
import os
import sys
import pprint
import logging
from contextlib import contextmanager
from decimal import Decimal

from sqlalchemy import select, join, func, and_

from bike_parts_loader.db import models
from config import Session

log = logging.getLogger(__name__)
FILE_PATH = os.path.abspath(os.path.join(__file__, os.pardir))
pp = pprint.PrettyPrinter(indent=4)


@contextmanager
def get_session():
    session = Session()
    yield session
    session.commit()


class DbManager(object):
    def _debug_save_item_to_file(self, item_info):
        log_file_path = os.path.join(FILE_PATH, 'files', 'extracted_items.log')
        with open(log_file_path, 'a') as f:
            pprint.pprint(item_info, stream=f)

    def _get_or_create_category(self, breadcrumbs, session):
        parent_category = None
        for category in breadcrumbs:
            this_category_qs = session.query(models.BPCategory).filter_by(url=category)
            cnt = this_category_qs.count()
            if cnt == 1:
                parent_category = this_category_qs.first()
                continue
            elif cnt == 0:
                parent_category = models.BPCategory(
                    parent_id=(parent_category and parent_category.id) or None,
                    name=category.split('/')[-2],
                    url=category
                )
                session.add(parent_category)
        return parent_category

    def _save_images(self, images, good, session):
        session.add_all([models.ImageLink(bp_good_id=good.id, url=url) for url in images])  # fixme: not saving foreign key

    def save_item_to_db(self, item_info):
        with get_session() as session:
            good = session.query(models.BPGood)\
                    .filter_by(item_number=item_info['item_number'])\
                    .first()
            if not good:
                category = self._get_or_create_category(item_info['breadcrumbs'], session)

                good = models.BPGood(
                    item_number=item_info['item_number'],
                    name=item_info['name'],
                    bp_category_id=category.id,
                    description_html=item_info['description_html']
                )
                session.add(good)
                good_created = True
                log.debug(u'Created new good: {}'.format(good.name))
            else:
                good_created = False

            session.flush()
            assert(good.id is not None)

            if not good_created:
                session.query(models.ImageLink)\
                       .filter(models.ImageLink.bp_good_id == good.id)\
                       .delete()

            self._save_images(item_info['images'], good, session)
            avail = models.Availability(availability=item_info['availability'], bp_good_id=good.id)
            session.add(avail)
            price = models.Price(price=Decimal(item_info['price']), bp_good_id=good.id)
            session.add(price)

    @staticmethod
    def refresh_mv(session=get_session()):
        models.BPGoodMatView.refresh(session, concurrently=False)



