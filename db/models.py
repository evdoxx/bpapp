# coding=utf-8
import logging

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, ForeignKey, String, Text, DateTime, Numeric, Index
from sqlalchemy import select, outerjoin, join, and_, func
from sqlalchemy.sql.functions import now

from mat_view_factory import create_mat_view, refresh_mat_view



from config import Base

log = logging.getLogger('sqlalchemy')
log.addHandler(logging.NullHandler())

# Base = declarative_base()
log.info('importing models')
print 'importing models'
def last_val_select(model):
    last_vals = select([model.bp_good_id, func.max(model.date).label('max_date')]) \
        .group_by(model.bp_good_id).select_from(model).alias('last_vals')
    joined = join(model, last_vals, and_(model.bp_good_id == last_vals.c.bp_good_id,
                                         model.date == last_vals.c.max_date))

    result = select([model]).select_from(joined)
    return result

class BPCategory(Base):
    __tablename__ = 'bp_categories'
    id = Column(Integer, primary_key=True)
    parent_id = Column(Integer, ForeignKey('bp_categories.id'))
    name = Column(String(1000))
    url = Column(String(1000), unique=True)

    goods = relationship('BPGood')
    parent = relationship('BPCategory')


class BPGood(Base):
    __tablename__ = 'bp_goods'
    id = Column(Integer, primary_key=True)
    item_number = Column(Integer)
    name = Column(String(1000))
    bp_category_id = Column(Integer, ForeignKey('bp_categories.id'))#
    #bp_category = relationship('Category', back_populates='bp_goods')
    description_html = Column(Text)

    availability = relationship('Availability', lazy='dynamic')
    price = relationship('Price', lazy='dynamic')
    images = relationship('ImageLink')
    category = relationship('BPCategory')


class Availability(Base):
    __tablename__ = 'avaliability'
    id = Column(Integer, primary_key=True)
    bp_good_id = Column(Integer, ForeignKey('bp_goods.id'), nullable=False)
    date = Column(DateTime, server_default=now())
    availability = Column(String(1000))


class Price(Base):
    __tablename__ = 'prices'
    id = Column(Integer, primary_key=True)
    bp_good_id = Column(Integer, ForeignKey('bp_goods.id'), nullable=False)
    date = Column(DateTime, server_default=now())
    price = Column(Numeric(asdecimal=True))


class ImageLink(Base):
    __tablename__ = 'image_links'
    id = Column(Integer, primary_key=True)
    bp_good_id = Column(Integer, ForeignKey('bp_goods.id'), nullable=False)
    url = Column(String(1000))


class BPGoodMatView(Base):
    last_prices = last_val_select(Price).alias('last_prices')
    last_avail = last_val_select(Availability).alias('last_avail')
    # first_photo = select([BP])

    number_of_photos = select([
        ImageLink.bp_good_id.label('bp_good_id'),
        func.count(ImageLink.id).label('cnt'),
        func.max(ImageLink.url).label('url')
    ]).select_from(ImageLink)\
        .group_by(ImageLink.bp_good_id).alias('number_of_photos')

    __table__ = create_mat_view(
        'bp_good_mv',
        select([
            BPGood.id.label('id'),
            BPGood.name.label('name'),
            last_prices.c.price.label('price'),
            last_avail.c.availability.label('availability'),
            BPCategory.id.label('category_id'),
            BPCategory.name.label('category_name'),
            func.coalesce(number_of_photos.c.cnt, 0).label('photos_cnt'),
            number_of_photos.c.url.label('photo')
        ]).select_from(
            outerjoin(BPGood, last_prices, last_prices.c.bp_good_id == BPGood.id)
            .outerjoin(last_avail, last_avail.c.bp_good_id == BPGood.id)
            .outerjoin(BPCategory, BPGood.bp_category_id == BPCategory.id)
            .outerjoin(number_of_photos, BPGood.id == number_of_photos.c.bp_good_id)
        ),
        Base.metadata
    )

    @classmethod
    def refresh(cls, session, concurrently=True):
        refresh_mat_view(cls.__table__.fullname, concurrently, session)


Index('goodmv_price_idx', BPGoodMatView.price)
Index('goodmv_cat_idx', BPGoodMatView.category_id)
Index('goodmv_avail_idx', BPGoodMatView.availability)
Index('goodmv_avail_idx', BPGoodMatView.photos_cnt)


from config import engine
# BPGoodMatView.__table__.create(bind=engine)
from sqlalchemy.exc import ProgrammingError
try:
    BPGoodMatView.metadata.create_all(engine)
except ProgrammingError as e:
    log.error('cant create MV. {}'.format(str(e.args)))



