"""initial revision

Revision ID: 58ae138abcef
Revises: 
Create Date: 2017-04-01 04:34:31.577306

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '58ae138abcef'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('bp_categories',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('parent_id', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=1000), nullable=True),
    sa.Column('url', sa.String(length=1000), nullable=True),
    sa.ForeignKeyConstraint(['parent_id'], ['bp_categories.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('bp_goods',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('item_number', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=1000), nullable=True),
    sa.Column('bp_category_id', sa.Integer(), nullable=True),
    sa.Column('description_html', sa.Text(), nullable=True),
    sa.ForeignKeyConstraint(['bp_category_id'], ['bp_categories.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('avaliability',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('bp_good_id', sa.Integer(), nullable=True),
    sa.Column('date', sa.DateTime(), nullable=True),
    sa.Column('availability', sa.String(length=1000), nullable=True),
    sa.ForeignKeyConstraint(['bp_good_id'], ['bp_goods.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('image_links',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('bp_good_id', sa.Integer(), nullable=True),
    sa.Column('url', sa.String(length=1000), nullable=True),
    sa.ForeignKeyConstraint(['bp_good_id'], ['bp_goods.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('prices',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('bp_good_id', sa.Integer(), nullable=True),
    sa.Column('date', sa.DateTime(), nullable=True),
    sa.Column('price', sa.Numeric(), nullable=True),
    sa.ForeignKeyConstraint(['bp_good_id'], ['bp_goods.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('prices')
    op.drop_table('image_links')
    op.drop_table('avaliability')
    op.drop_table('bp_goods')
    op.drop_table('bp_categories')
    # ### end Alembic commands ###
