# coding=utf-8
import json
import logging

from bike_parts_loader.db import models
from bike_parts_loader import settings

log = logging.getLogger(__name__)


def serialize_good(good_model):
    try:
        result = {
            'id': good_model.id,
            'name': good_model.name,
            'category_name': good_model.category.name,
            'price': float(good_model.price.order_by(models.Price.date).first().price),
            'availability': good_model.availability.order_by(models.Availability.date)\
                .first().availability,
            'image_url': good_model.images[0].url if len(good_model.images) else '/static/nopic.png'
        }
    except (AttributeError, IndexError) as e:
        log.warning('Error processing goods: {}'.format(str(e.args)))
        return None

    return result


def serialize_good_from_joined(result):
    try:
        good = result.BPGood
        result = {
            'id': good.id,
            'name': good.name,
            'category_name': good.category.name,
            'price': float(result.price),
            'availability': str(result.availability)
            # 'availability': good_model.availability.order_by(models.Availability.date)\
            #     .first().availability,
            # 'image_url': result.images[0].url if len(result.images) else '/static/nopic.png'
        }
    except (AttributeError, IndexError) as e:
        log.warning('Error processing goods: {}'.format(str(e.args)))
        return None


def serialize_from_mat_view(good):
    result = {
        'id': good.id,
        'name': good.name,
        'price': float(good.price),
        'category_name': good.category_name,
        'availability': good.availability,
        'image_url': good.photo
    }
    return result


def extract_goods_filters(request):
    args = request.args
    avail_map = {'false': False, 'true': True}

    filters = {
        'category_id': _t(lambda: int(args['category_id'])),
        'limit': max(0, min(int(args.get('limit', 12)), 100)),
        'offset': int(args.get('offset', 0)),
        'price_max': _t(lambda: float(args['price_max'])),
        'price_min': _t(lambda: float(args['price_min'])),
        'avail': _t(lambda: avail_map[str(args['stock_only'])]),
        'mode': args.get('mode', 'items'),
        'photos_min': _t(lambda: int(args.get('photos_min'))),
        'photos_max': _t(lambda: int(args.get('photos_max')))
    }
    return filters


def try_or_none(func):
    try:
        return func()
    except Exception as e:
        log.exception('try_or_none error')
        return None