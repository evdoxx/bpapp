# coding=utf-8

import os
import json
from collections import defaultdict

from flask import Flask
from flask import request, make_response

from bike_parts_loader.settings import *
import bike_parts_loader.settings
from bike_parts_loader.db import models
from bike_parts_loader.webapp.helpers import serialize_from_mat_view, extract_goods_filters
from bike_parts_loader.db.db_manager import get_session

log = logging.getLogger(__name__)
FILE_PATH = os.path.dirname(os.path.abspath(__file__))
app = Flask(
                'bike-parts',
                static_url_path='/static',
                static_folder=os.path.join(dev.MODULE_PATH, os.pardir, 'static'),
            )


@app.route('/')
def main():
    return app.send_static_file('html/index.html')


@app.route('/api/goods', methods=['GET'])
def goods():
    if request.method == 'GET':
        try:
            filters = extract_goods_filters(request)
        except ValueError as e:
            return 'err', 400
        with get_session() as s:
            goods_qs = s.query(models.BPGoodMatView).order_by(models.BPGoodMatView.id)
            if filters['category_id']:
                goods_qs = goods_qs.filter(models.BPGoodMatView.category_id == filters['category_id'])
            if filters['price_max'] is not None:
                goods_qs = goods_qs.filter(models.BPGoodMatView.price <= filters['price_max'])
            if filters['price_min'] is not None:
                goods_qs = goods_qs.filter(models.BPGoodMatView.price >= filters['price_min'])
            if filters['avail']:
                goods_qs = goods_qs.filter(models.BPGoodMatView.availability == 'in stock')
            if filters['photos_max'] is not None:
                goods_qs = goods_qs.filter(models.BPGoodMatView.photos_cnt <= filters['photos_max'])
            if filters['photos_min'] is not None:
                goods_qs = goods_qs.filter(models.BPGoodMatView.photos_cnt >= filters['photos_min'])

        if filters['mode'] == 'items':
            goods_qs = goods_qs.offset(filters['offset']).limit(filters['limit'])
            serialized_goods = [serialize_from_mat_view(good) for good in goods_qs]
            response_payload = {'goods':  [good for good in serialized_goods if good]}
        elif filters['mode'] == 'count':
            response_payload = {'count': goods_qs.count()}
        else:
            response_payload = dict()

        response = make_response(json.dumps(response_payload))
        response.headers['Content-Type'] = 'application/json'
        return response
    else:
        return 'method not allowed', 400


@app.route('/api/categories', methods=['GET'])
def categories():
    with get_session() as s:
        top_cats = get_children_categories(s)
        cats = {'categories': [get_category_tree(model, s) for model in top_cats]}

    response = make_response(json.dumps(cats))
    response.headers['Content-Type'] = 'application/json'
    return response


def get_category_tree(category_model, session):
    children = get_children_categories(session, parent=category_model.id)
    result = {
        'name': category_model.name,
        'id': category_model.id,
    }
    if children:
        result['children'] = [{'name': child.name, 'id': child.id} for child in children]
    return result


def get_children_categories(session, parent=None):
    return session.query(models.BPCategory)\
        .filter(models.BPCategory.parent_id == parent)\
        .order_by(models.BPCategory.id).all()


if __name__ == '__main__':
    app.run(host='0.0.0.0')
