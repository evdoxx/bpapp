# coding=utf-8

from bike_parts_loader import settings
from bike_parts_loader.db.models import *

import logging

log = logging.getLogger(__name__)



def main():
    with settings.get_session() as s:
        #queries
        goods = s.query(BPGood)
        cnt = goods.count()
        goods2 = goods.filter(BPGood.bp_category_id > 100)
        cnt2 = goods2.count()

        gfs = goods.from_self()
        cnt11 = gfs.count()
        gsq = goods.subquery()

        goods_columns = goods.column_descriptions
        goods_from_self_columns = gfs.column_descriptions


        #joins
        goods_simple_join = goods.join(BPCategory)
        goods_simple_join_columns = goods_simple_join.column_descriptions

        # goods_self
        ##info retrieval

        for item in goods:
            print item.name, item.item_number, item.category.name
            break

        for item in goods:
            a = item.category.goods
            break

        for item in goods_simple_join:
            print item.name, item.item_number, item.category.name
            break

        #query -> define fileds
        changin_column_output = goods.from_self(BPGood.id, BPGood.name)

        #iterating specific columns
        for item in changin_column_output:
            print item.id, item.name
            break
        #query -> join -> query tecnique

        changin_output_after_join = goods_simple_join.from_self(BPGood, BPCategory)
        changin_output_before_join = goods.from_self(BPGood, BPCategory)
        after_count = changin_output_after_join.count()
        before_count = changin_output_before_join.count()

        ## same shit!!! both incorrect
        print '-----------'
        print goods_simple_join.count()
        changin_output_after_join1 = goods_simple_join.add_entity(BPCategory).from_self(BPGood, BPCategory) #correct
        changin_output_before_join1 = goods.add_entity(BPCategory).from_self(BPGood, BPCategory) #creates unconditional join
        after_count1 = changin_output_after_join1.count()
        before_count1 = changin_output_before_join1.count()

        pass
from sqlalchemy.orm import aliased
from sqlalchemy import alias
def subqueries():
    with settings.get_session() as s:
        goods = s.query(BPGood)
        print 'ggos count {}'.format(goods.count())
        test_prefix = goods.subquery('test_prefix') #this is new name

        aliased_cats = s.query(BPCategory).filter(BPCategory.id > 50).subquery('aliased_cats')
        c = aliased_cats.count()
        print 'cats'
        # goods_aliased = aliased(goods) you cant alias query
        goods_sub_aliased = aliased(test_prefix)

        ##join with self

        # goods_naive_selfjoin = goods.join(goods, goods.id == goods.id) # 'Query' object has no attribute 'id'
        # join_with_selectable = goods.join(test_prefix, BPGood.id == test_prefix.id) # alias has no attribute id
        join_with_selectable = goods.join(test_prefix, BPGood.id == test_prefix.c.id)
        print join_with_selectable.count()
        #acessing columns
        for item in join_with_selectable:
            print item.id#, item.test_prefix.name # item is BPGood
            break
        #choosing columns

        # j2 = join_with_selectable.add_entity(test_prefix.selectable) #-- AttributeError: 'Alias' object has no attribute 'is_aliased_class'
        j1 = goods.join(aliased_cats, BPGood.bp_category_id == aliased_cats.c.id)
        print 'j1.count()', j1.count()
        # j2 = j1.add_entity(BPCategory) ## unconditional join
        #j3 = j1.add_entity(aliased_cats)
        j2 = j1.add_column(aliased_cats) #works but adds column to global result namespace
        print j2.count()
        j3 = j1.add_columns(aliased_cats.c.name.label('cat_name'), aliased_cats.c.url.label('cat_url'))
        print j3.count()
        for item in j3:
            pass
            print item.id, item.test_prefix.name#, item.test_prefix.name # item is BPGood
        pass


if __name__ == '__main__':
    subqueries()

