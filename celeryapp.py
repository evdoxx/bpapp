# coding=utf-8
from __future__ import absolute_import, unicode_literals
import logging
from celery import Celery

# import bike_parts_loader.settings

log = logging.getLogger(__name__)
print 'celery app start'

app = Celery(
    'bike-parts',
    broker='amqp://localhost:5672',
    include=['bike_parts_loader.bikepartsloader']
)

app.config_from_object('bike_parts_loader.settings.celeryconfig') #fixme
if __name__ == '__main__':
    app.start()